﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class TotalCompteur
    {
        public int incrementer;
        public int decrementer;
        public int reboot;

        public TotalCompteur(int incrementer, int decrementer, int reboot)
        {
            this.incrementer = incrementer;
            this.decrementer = decrementer;
            this.reboot = reboot;
        }

        public int setIncrementer()
        {
            this.incrementer = incrementer + 1;
            return this.incrementer;
        }

        public int setDecrementer()
        {
            this.decrementer = decrementer - 1;
            return this.incrementer;
        }

        public int setReboot()
        {
            this.reboot = 0;
            return this.reboot;
        }
    }
}
